#!/usr/bin/env python

TREE = '#'

grid = []
with open("input", "r") as f:
    for iy, y in enumerate(f):
        grid.insert(iy, list(y.rstrip()))
width = len(grid[0])

def ride(gx=3, gy=1):
    x = 0
    y = 0
    trees = 0
    while True:
        try:
            if grid[y][x] == TREE:
                trees += 1
            x = (x + gx) % width
            y += gy
        except IndexError as ex:
            return trees

print(ride())
print(ride(1,1) * ride(3, 1) * ride(5, 1) * ride(7, 1) * ride(1, 2))
