import numpy as np

with open("input", "r") as f:
    # lines = f.read().split("\n")[:-1]
    lines = [l.rstrip() for l in f if l.strip()]

def get_seat(specifier):
    pre, post = specifier[:7], specifier[7:]
    def get_posn(sp): 
        return sum(2 ** i if d else 0 for i, d in enumerate(reversed(sp)))
    row_no = get_posn([ch == "B" for ch in pre])
    col_no = get_posn([ch == "R" for ch in post])
    id = row_no * 8 + col_no
    return row_no, col_no, id

def seat_cringe(specifiers):
    ls = [True] * (128 * 8)
    for specifier in specifiers:
        _, _, id = get_seat(specifier)
        ls[id] = False
    for n,l in enumerate(ls):
        if l:
            # print(n,l)
            pass
    print(ls.index(False))
    breakpoint()
    return ls.index(True, ls.index(False) + 1)

seats = [get_seat(l) for l in lines]
seats = sorted(seats, key=lambda t: t[2])
print(seats[-1])
hi = seats[-1][2]
lo = seats[0][2]

print(seat_cringe(lines))
