#!/usr/bin/env stack
-- stack --resolver lts-16.24 script --package text

{-# LANGUAGE OverloadedStrings #-}

import Data.Function (on)
import Data.List
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

data BoardingPass = BoardingPass
  { row :: Int,
    col :: Int,
    sid :: Int
  }
  deriving (Show)

readInput :: IO [String]
readInput = lines <$> readFile "input"

posn :: [Bool] -> Int
posn spec =
  foldr (+) 0 $
    map
      (\(i, d) -> if d then 2 ^ i else 0)
      (zip [len -1, len -2 .. 0] spec)
  where
    len = length spec

lineToPass :: String -> BoardingPass
lineToPass l =
  let row = posn $ map (== 'B') $ take 7 l
      col = posn $ map (== 'R') $ drop 7 l
      sid = row * 8 + col
   in BoardingPass {row = row, col = col, sid = sid}

main :: IO ()
main = do
  input <- readInput
  let passes = map lineToPass input
  -- mapM_ (putStrLn . show) passes
  let sorted = sortBy (flip compare `on` sid) passes
  putStrLn . show $ head sorted

  let lowsid = sid . head . reverse $ sorted
  let hisid = sid . head $ sorted
  let seats = [(r, c) | r <- [0 .. 127], c <- [0 .. 7]]
  let filledSeats = map (\p -> (row p, col p)) passes
  let unfilledSeats = seats \\ filledSeats
  let sids = map (\(r, c) -> r * 8 + c) unfilledSeats
  let realsids = filter (\sid -> sid >= lowsid && sid <= hisid) sids
  print realsids
