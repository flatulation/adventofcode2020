#!/usr/bin/env python3
numbers = []
with open("input", "r") as f:
    for number in f.readlines():
        numbers.append(int(number.rstrip()))

for number in numbers:
    diff = 2020 - number
    if diff in numbers:
        print(number, "×", diff, "=", number * diff)
        break

def pt2():
    for number in numbers:
        diff = 2020 - number
        nums2 = list(filter(lambda n: n < diff, numbers))
        for n1, i in enumerate(nums2):
            for j in nums2[n1:]:
                if i + j == diff:
                    print(number, "+", i, "+", j, "=", number + i + j)
                    print(number, "×", i, "×", j, "=", number * i * j)
                    return

pt2()
