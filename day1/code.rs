use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

type uint = u16;

fn read_input() -> io::Result<Vec<uint>> {
    let f = File::open("input")?;
    let rd = BufReader::new(f);
    let vec = rd
        .lines()
        .filter_map(|r| r.ok())
        .filter_map(|r| r.parse::<uint>().ok())
        .collect();
    Ok(vec)
}

fn pt1(numbers: &[uint]) {
    for number in numbers.iter() {
        let diff = 2020 - number;
        if numbers.contains(&diff) {
            let result = *number as u32 * diff as u32;
            println!("{} × {} = {}", number, diff, result);
            break;
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let numbers = read_input()?;
    pt1(&numbers);
    Ok(())
}
