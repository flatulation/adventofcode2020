from enum import IntEnum
import random
import time


class Opcode(IntEnum):
    NOP = 0
    ACC = 1
    JMP = 2

    @staticmethod
    def from_mnem(mnem):
        if mnem == "nop":
            return Opcode.NOP
        elif mnem == "acc":
            return Opcode.ACC
        elif mnem == "jmp":
            return Opcode.JMP
        else:
            raise ValueError


class ProcFaultedError(Exception):
    def __init__(self, *args, pt1=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.pt1 = pt1


class EndOfInstructionStreamError(Exception):
    pass


class Proc:
    def __init__(self, istream, *, debug=False, pt1=False, pt2=False) -> None:
        self._istream = list(istream) if isinstance(istream, tuple) else istream.copy()
        self.reset()
        self.pt1 = pt1
        self.pt2 = pt2 != False
        self.debug = debug
        self.has_flipped = []
        try:
            self.interval = int(pt2)
        except ValueError:
            self.interval = 1

    def reset(self):
        self.istream = self._istream.copy()
        self.accr = self.ip = 0
        self.faulted = self.flipped = False
        self.ips = []

    def dprint(self, msg):
        if self.debug:
            print(msg)

    def acc(self, oper):
        self.accr += oper
        self.dprint(
            f"${self.ip}: acc {oper} (accr = {self.accr - oper} + {oper} = {self.accr})"
        )
        self.ip += 1

    def jmp(self, oper):
        self.dprint(f"${self.ip}: jmp {oper} -> ${self.ip + oper}")
        self.ip += oper

    def fault(self):
        self.faulted = True
        raise ProcFaultedError(
            f"faulted! ip = ${self.ip} accr = {self.accr}", pt1=self.pt1
        )

    def patch(self, ip, op1, op2):
        op, oper = self.istream[ip]
        if op != op1:
            raise ValueError("op1 does not match!")
        self.istream[ip] = (op2, oper)
        print(f"${ip} patched from {op} to {op2}")  # always print

    def step(self):
        if self.faulted:
            raise ProcFaultedError

        try:
            op, oper = self.istream[self.ip]
        except IndexError as e:
            raise EndOfInstructionStreamError from e

        # didnt happen in my input
        if (
            self.pt2
            and not self.flipped
            and self.ip not in self.has_flipped
            and self.ip + oper == len(self.istream)
            and op == Opcode.NOP
        ):
            self.patch(self.ip, Opcode.NOP, Opcode.JMP)
            self.has_flipped.append(self.ip)
            self.flipped = True
            return self.step()

        # brute force the wrong jmp
        if (
            self.pt2
            and not self.flipped
            and op == Opcode.JMP
            and self.ip not in self.has_flipped
            and random.random > 0.5
        ):
            self.patch(self.ip, Opcode.JMP, Opcode.NOP)
            self.has_flipped.append(self.ip)
            self.flipped = True
            return self.step()

        if self.pt1:
            self.ips.append(self.ip)

        if op == Opcode.NOP:
            self.dprint(f"${self.ip}: nop")
            self.ip += 1
        elif op == Opcode.ACC:
            self.acc(oper)
        elif op == Opcode.JMP:
            self.jmp(oper)
        else:
            self.fault()

        if self.pt1 and self.ip in self.ips:
            self.fault()

        if self.pt2 and self.started + self.interval < time.time():
            print("reset!")
            self.started = time.time()
            cpu.reset()

    def run(self):
        self.started = time.time()
        while True:
            self.step()


def parse(fp):
    with open(fp, "r") as f:
        return tuple(
            (Opcode.from_mnem(p[0]), int(p[1]))
            for p in (l.rstrip().split(" ") for l in f)
        )


istns = parse("input")

cpu = Proc(istns, pt1=True)
try:
    cpu.run()
except ProcFaultedError as ex:
    if ex.pt1:
        print("pt1:", cpu.accr)
    else:
        raise

cpu = Proc(istns)
# bruteforced. pass pt2=True to Proc constructor to do the bruteforcing
cpu.patch(210, Opcode.JMP, Opcode.NOP)
try:
    cpu.run()
except EndOfInstructionStreamError:
    print("pt2:", cpu.accr)
