#!/usr/bin/env ruby
Row = Struct.new(:min, :max, :char, :passwd)

rows = File.open("input") do |f|
  pattern = /^(\d+)-(\d+) (.): (.+)/
  f.map do |l|
    matches = pattern.match(l)
    Row.new(matches[1].to_i, matches[2].to_i, matches[3], matches[4])
  end
end

puts (rows.map do |row|
  count = row.passwd.count(row.char)
  count >= row.min && count <= row.max
end.sum {|b| b ? 1 : 0}) #.map {|b| b ? 1 : 0}

puts (rows.reduce(0) do |i, row| 
  (row.passwd[row.min - 1] == row.char) != (row.passwd[row.max - 1] == row.char) ? i + 1 : i
end)
