def read_input():
    with open("input.txt") as f:
        return {
            outer: tuple(
                (int(ws[0]), " ".join(ws[1:]))
                for ws in (
                    colour.strip().replace(".", "").split(" ")
                    for colour in inners.split(",")
                )
                if ws[0] != "no"
            )
            for outer, inners in (
                l.replace(" bags", "").replace(" bag", "").split(" contain ")
                for l in f.read().split("\n")
                if l
            )
        }


colours = read_input()

from functools import reduce
from io import UnsupportedOperation

holds_shiny_gold = {
    outer for outer, inners in colours.items() if "shiny gold" in (t[1] for t in inners)
}

while True:
    new_colours = reduce(
        lambda s1, s2: s1.union(s2),
        (
            {outer for col in (t[1] for t in inners) if col in holds_shiny_gold}
            for outer, inners in colours.items()
            if outer not in holds_shiny_gold
        ),
        set(),
    )
    if not new_colours:
        break
    holds_shiny_gold = holds_shiny_gold.union(new_colours)
print("part 1:", len(holds_shiny_gold))


def n_contained(col) -> int:
    acc = 0
    for n, col2 in colours[col]:
        acc += n + n * n_contained(col2)
    return acc
