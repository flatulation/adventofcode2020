#!/usr/bin/env python
import re

def check_passport(passport):
    for k in ("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"):
        if k not in passport:
            return False
    return True

def validate_byr(byr):
    try:
        byr = int(byr)
        if byr < 1920 or byr > 2002:
            return False
    except ValueError:
        return False
    return True

def validate_iyr(iyr):
    try:
        iyr = int(iyr)
        if iyr < 2010 or iyr > 2020:
            return False
    except ValueError:
        return False
    return True

def validate_eyr(eyr):
    try:
        eyr = int(eyr)
        if eyr < 2020 or eyr > 2030:
            return False
    except ValueError:
        return False
    return True

height_regex = re.compile("(\d+)(cm|in)")
def validate_hgt(hgt):
    matches = height_regex.search(hgt)
    if not matches:
        return False
    height = int(matches.group(1))
    unit = matches.group(2)
    if unit == "cm" and (height < 150 or height > 193):
        return False
    elif unit == "in" and (height < 59 or height > 76):
        return False
    return True

colour_regex = re.compile("#([a-f0-9]{6})")
def validate_hcl(hcl):
    if not colour_regex.match(hcl):
        return False
    return True

def validate_ecl(ecl):
    if ecl not in ("amb", "blu", "brn", "gry", "grn", "hzl", "oth"):
        return False
    if len(ecl) != 3:
        return False
    return True

pid_regex = re.compile("\d{9}$")
def validate_pid(pid):
    if not pid_regex.match(pid):
        return False
    return True

def validate_passport(passport):
    if not validate_ecl(passport["ecl"]):
        return False

    if not validate_hgt(passport["hgt"]):
        return False

    if not validate_hcl(passport["hcl"]):
        return False

    if not validate_byr(passport["byr"]):
        return False

    if not validate_iyr(passport["iyr"]):
        return False

    if not validate_eyr(passport["eyr"]):
        return False

    if not validate_pid(passport["pid"]):
        return False

    return True
    
def read_input():
    with open("input", "r") as f:
        dat = f.read()
    def parse(p):
        kvs = re.split(" |\n", p)
        d = {}
        for kv in kvs:
            try:
                k, v = kv.split(':')
                d[k] = v
            except ValueError:
                continue
        return d
    passports = map(parse, dat.split("\n\n"))
    return passports

passports = read_input()
checked = list(filter(check_passport, passports))
print(len(checked))
validated = list(filter(validate_passport, checked))
print(len(validated))