use std::fs::File;
use std::io::{self, BufRead};

fn parse(path: &str) -> io::Result<Vec<u64>> {
    Ok(io::BufReader::new(File::open(path)?)
        .lines()
        .filter_map(|rs| rs.ok())
        .filter_map(|s| s.parse().ok())
        .collect())
}

fn pt1(nums: &[u64]) -> u64 {
    for (i, &c) in nums.iter().enumerate() {
        if i < 25 {
            continue;
        }
        let prev = &nums[(i - 25)..i];
        if !prev
            .iter()
            .filter_map(|n| c.checked_sub(*n))
            .any(|x| prev.contains(&x))
        {
            return c;
        }
    }
    panic!("wut")
}

fn pt2(nums: &[u64], pt1_result: u64) -> u64 {
    let mut start = 0;
    let mut offset = 0;
    let mut sum = nums[start];
    loop {
        offset += 1;
        sum += nums[start + offset];
        if sum > pt1_result {
            start += 1;
            offset = 0;
            sum = nums[start];
        } else if sum == pt1_result {
            let mut range = (&nums[start..(start + offset)]).to_owned();
            range.sort();
            let min = range.first().unwrap();
            let max = range.last().unwrap();
            return min + max;
        }
    }
}

fn main() {
    let nums = parse("input.txt").expect("couldn't parse input!");

    let pt1_result = pt1(&nums);
    println!("pt1: {}", pt1_result);

    println!("pt2: {}", pt2(&nums, pt1_result));
}
