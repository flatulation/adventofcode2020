#!/bin/sh
if [[ code.rs -nt code ]]; then
    rustc -C panic=abort code.rs || exit $?
fi
./code