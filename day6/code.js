const fs = require("fs");

const f = fs.readFileSync("input.txt", "utf-8")
const forms = f.split("\n\n").map(l => l.replace(/\s+/g, ''))

const responses = []
for (const f of forms) {
  const response = {}
  for (const c of f) {
    response[c] = true
  }
  responses.push(response)
}

const questions = {}
for (const response of Object.values(responses)) {
  for (const [q, n] of Object.entries(response)) {
    questions[q] = questions[q] || 0
    questions[q] += n
  }
}
console.log(Object.values(questions).reduce((x, y) => x + y))

/* (async () => {

})().catch(err => {
  console.error(err)
  process.exit(1)
}) */
