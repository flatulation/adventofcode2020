const fs = require("fs");

const f = fs.readFileSync("input.txt", "utf-8")
const forms = f.split("\n\n").map(f => f.trimEnd().split("\n"))

acc = forms.map(f => {
  const np = f.length
  const questions = {}
  for (const p of f) {
    for (const c of p) {
      questions[c] = questions[c] || 0
      questions[c]++
    }
  }
  return Object.values(questions).filter(n => n == np).length
}).reduce((x,y)=>x+y)

console.log(acc)